﻿using System;

namespace TMS
{
	public interface IServiceConsumer
	{
		/// <summary>
		/// Sets the service URL to consume
		/// </summary>
		/// <param name="URL"></param>
		void SetServiceURL( string URL );
	}
}