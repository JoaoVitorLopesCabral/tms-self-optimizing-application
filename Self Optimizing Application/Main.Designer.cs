﻿namespace TMS
{
	partial class Main
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			this.logServer1 = new System.Windows.Forms.ListBox();
			this.StartBtn = new System.Windows.Forms.Button();
			this.StopBtn = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.LoadServicesBtn = new System.Windows.Forms.Button();
			this.ServiceListBox = new System.Windows.Forms.ListBox();
			this.ServiceGroupBox = new System.Windows.Forms.GroupBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.CurrentServiceLbl = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.PerformanceChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.UseMonitorCHK = new System.Windows.Forms.CheckBox();
			this.ServiceGroupBox.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PerformanceChart)).BeginInit();
			this.SuspendLayout();
			// 
			// logServer1
			// 
			this.logServer1.FormattingEnabled = true;
			this.logServer1.Location = new System.Drawing.Point(13, 341);
			this.logServer1.Name = "logServer1";
			this.logServer1.Size = new System.Drawing.Size(648, 95);
			this.logServer1.TabIndex = 1;
			// 
			// StartBtn
			// 
			this.StartBtn.Location = new System.Drawing.Point(5, 18);
			this.StartBtn.Name = "StartBtn";
			this.StartBtn.Size = new System.Drawing.Size(75, 23);
			this.StartBtn.TabIndex = 2;
			this.StartBtn.Text = "Start";
			this.StartBtn.UseVisualStyleBackColor = true;
			this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
			// 
			// StopBtn
			// 
			this.StopBtn.Location = new System.Drawing.Point(86, 18);
			this.StopBtn.Name = "StopBtn";
			this.StopBtn.Size = new System.Drawing.Size(75, 23);
			this.StopBtn.TabIndex = 3;
			this.StopBtn.Text = "Stop";
			this.StopBtn.UseVisualStyleBackColor = true;
			this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(596, 314);
			this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 19);
			this.button1.TabIndex = 4;
			this.button1.Text = "dump";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// LoadServicesBtn
			// 
			this.LoadServicesBtn.Location = new System.Drawing.Point(4, 30);
			this.LoadServicesBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.LoadServicesBtn.Name = "LoadServicesBtn";
			this.LoadServicesBtn.Size = new System.Drawing.Size(104, 27);
			this.LoadServicesBtn.TabIndex = 5;
			this.LoadServicesBtn.Text = "Load Services";
			this.LoadServicesBtn.UseVisualStyleBackColor = true;
			this.LoadServicesBtn.Click += new System.EventHandler(this.LoadServicesBtn_Click);
			// 
			// ServiceListBox
			// 
			this.ServiceListBox.FormattingEnabled = true;
			this.ServiceListBox.Location = new System.Drawing.Point(113, 30);
			this.ServiceListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.ServiceListBox.Name = "ServiceListBox";
			this.ServiceListBox.Size = new System.Drawing.Size(527, 108);
			this.ServiceListBox.TabIndex = 6;
			// 
			// ServiceGroupBox
			// 
			this.ServiceGroupBox.Controls.Add(this.ServiceListBox);
			this.ServiceGroupBox.Controls.Add(this.LoadServicesBtn);
			this.ServiceGroupBox.Location = new System.Drawing.Point(13, 10);
			this.ServiceGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.ServiceGroupBox.Name = "ServiceGroupBox";
			this.ServiceGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.ServiceGroupBox.Size = new System.Drawing.Size(651, 150);
			this.ServiceGroupBox.TabIndex = 7;
			this.ServiceGroupBox.TabStop = false;
			this.ServiceGroupBox.Text = "Services";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.UseMonitorCHK);
			this.groupBox1.Controls.Add(this.CurrentServiceLbl);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.StartBtn);
			this.groupBox1.Controls.Add(this.StopBtn);
			this.groupBox1.Location = new System.Drawing.Point(13, 165);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox1.Size = new System.Drawing.Size(651, 76);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Application";
			// 
			// CurrentServiceLbl
			// 
			this.CurrentServiceLbl.AutoSize = true;
			this.CurrentServiceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CurrentServiceLbl.ForeColor = System.Drawing.Color.SeaGreen;
			this.CurrentServiceLbl.Location = new System.Drawing.Point(254, 23);
			this.CurrentServiceLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.CurrentServiceLbl.Name = "CurrentServiceLbl";
			this.CurrentServiceLbl.Size = new System.Drawing.Size(22, 13);
			this.CurrentServiceLbl.TabIndex = 4;
			this.CurrentServiceLbl.Text = "#0";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(166, 23);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Current Service:";
			// 
			// PerformanceChart
			// 
			chartArea3.Name = "ChartArea1";
			this.PerformanceChart.ChartAreas.Add(chartArea3);
			legend3.Name = "Legend1";
			this.PerformanceChart.Legends.Add(legend3);
			this.PerformanceChart.Location = new System.Drawing.Point(669, 12);
			this.PerformanceChart.Name = "PerformanceChart";
			this.PerformanceChart.Size = new System.Drawing.Size(651, 424);
			this.PerformanceChart.TabIndex = 9;
			this.PerformanceChart.Text = "chart1";
			// 
			// UseMonitorCHK
			// 
			this.UseMonitorCHK.AutoSize = true;
			this.UseMonitorCHK.Checked = true;
			this.UseMonitorCHK.CheckState = System.Windows.Forms.CheckState.Checked;
			this.UseMonitorCHK.Location = new System.Drawing.Point(4, 47);
			this.UseMonitorCHK.Name = "UseMonitorCHK";
			this.UseMonitorCHK.Size = new System.Drawing.Size(122, 17);
			this.UseMonitorCHK.TabIndex = 5;
			this.UseMonitorCHK.Text = "User service monitor";
			this.UseMonitorCHK.UseVisualStyleBackColor = true;
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1332, 450);
			this.Controls.Add(this.PerformanceChart);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.ServiceGroupBox);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.logServer1);
			this.Name = "Main";
			this.Text = "Service consuming application";
			this.ServiceGroupBox.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PerformanceChart)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox logServer1;
		private System.Windows.Forms.Button StartBtn;
		private System.Windows.Forms.Button StopBtn;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button LoadServicesBtn;
		private System.Windows.Forms.ListBox ServiceListBox;
		private System.Windows.Forms.GroupBox ServiceGroupBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label CurrentServiceLbl;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataVisualization.Charting.Chart PerformanceChart;
		private System.Windows.Forms.CheckBox UseMonitorCHK;
	}
}

