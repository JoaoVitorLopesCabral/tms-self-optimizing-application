﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TMS.Monitor;
using System.Windows.Forms.DataVisualization.Charting;
using System.Threading;
using System.Globalization;

namespace TMS
{
	public partial class Main : Form, IServiceConsumer
	{
		public Main()
		{
			InitializeComponent();
		}

		ServicePerformanceMonitor ServiceAnalyser { get; set; }
		string CurrentServiceURL { get; set; }
		List<ServiceModel> ServiceList { get; set; }
		bool IsRunning { get; set; }

		private void StartBtn_Click( object sender, EventArgs e )
		{
			// Set the first service as current
			ServiceModel Service = ServiceList.FirstOrDefault();
			CurrentServiceLbl.Text = Service.Name;
			CurrentServiceURL = Service.URL;

			ServiceAnalyser = new ServicePerformanceMonitor( ServiceList, this );
			ServiceAnalyser.PerformanceRecordCreated += ServiceAnalyser_PerformanceRecordCreated;
			ServiceAnalyser.StartRecorder();

			// Mark as running
			IsRunning = true;

			// Setup communication
			BackgroundWorker bwComm = new BackgroundWorker();
			bwComm.DoWork += BwComm_DoWork;
			bwComm.RunWorkerAsync();
		}
		/// <summary>
		/// Handles app communication with service
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void BwComm_DoWork( object sender, DoWorkEventArgs e )
		{
			while ( IsRunning )
			{
				Thread.Sleep( 1000 );
				// Retrieve best working service
				string ServiceURL = CurrentServiceURL;

				if ( string.IsNullOrWhiteSpace( ServiceURL ) )
				{
					continue;
				}

				using ( WebClient webClient = new WebClient() )
				{
					try
					{
						DateTime requestTime = DateTime.Now;

						using ( Stream webStream = webClient.OpenRead( ServiceURL ) )
						{
							byte[] buffer = new byte[ 100 ];
							int bytesRead = webStream.Read( buffer, 0, buffer.Length );

							// Update current service
							UpdateService( ServiceList.First( m => m.URL == ServiceURL ) );
						}

						DateTime responseTime = DateTime.Now;

						// Add to chart
						AddToChart( "Service Latency", ( responseTime - requestTime ).TotalMilliseconds );
						// Also add to mongo
						MongoContext.InsertRecord( "app", new ServicePerformanceRecordModel()
						{
							SendTime = requestTime,
							ReceiveTime = responseTime,
							ServiceURL = ServiceURL,
							TotalMilliseconds = ( responseTime - requestTime ).TotalMilliseconds,
							TotalMillisecondsString = ( responseTime - requestTime ).TotalMilliseconds.ToString( new NumberFormatInfo() { NumberDecimalSeparator = "," } ),
                            ServiceName = ServiceList.Where( S => S.URL == ServiceURL ).FirstOrDefault().Name
						} );
					}
					catch ( WebException ex )
					{
						// TODO: Log error
					}
				}
			}
		}

		public delegate void AddToChartCallback( string SeriesName, double Value );
		public void AddToChart( string SeriesName, double Value )
		{
			if ( PerformanceChart.InvokeRequired )
			{
				AddToChartCallback safe = new AddToChartCallback( AddToChart );
				Invoke( safe, new object[] { SeriesName, Value } );
			}
			else
			{
				var DataSerie = PerformanceChart.Series.FirstOrDefault( S => S.Name == SeriesName );
				if ( DataSerie != null )
				{
					DataSerie.Points.AddY( Value );
				}
				else
				{
					Series NewSerie = new Series( SeriesName );
					NewSerie.ChartType = SeriesChartType.Spline;
					NewSerie.Points.Add( Value );
					PerformanceChart.Series.Add( NewSerie );
				}
			}
		}

		public delegate void UpdateComponentCallback( string Value );
		public void UpdateList( string Value )
		{
			if ( logServer1.InvokeRequired )
			{
				UpdateComponentCallback safe = new UpdateComponentCallback( UpdateList );
				Invoke( safe, new object[] { Value } );
			}
			else
			{
				logServer1.Items.Add( Value );
			}
		}
		public delegate void UpdateServiceCallback( ServiceModel Service );
		public void UpdateService( ServiceModel Service )
		{
			if ( CurrentServiceLbl.InvokeRequired )
			{
				UpdateServiceCallback safe = new UpdateServiceCallback( UpdateService );
				Invoke( safe, new object[] { Service } );
			}
			else
			{
				CurrentServiceLbl.Text = Service.Name;
			}
		}

		private void ServiceAnalyser_PerformanceRecordCreated( ServiceRecorderEventArgs Args )
		{
			UpdateList( Args.Record.ToString() );

			// Add to mongo
			MongoContext.InsertRecord( "perf", Args.Record );
		}

		private void StopBtn_Click( object sender, EventArgs e )
		{
			ServiceAnalyser.StopRecorder();
			IsRunning = false;
		}

		private void button1_Click( object sender, EventArgs e )
		{
			using ( StreamWriter sw = new StreamWriter( string.Format( @"{0}\log.txt", Environment.GetFolderPath( Environment.SpecialFolder.DesktopDirectory ) ) ) )
			{
				sw.Write( ServiceAnalyser.GetBestService() );
				sw.Close();
			}
		}

		private void LoadServicesBtn_Click( object sender, EventArgs e )
		{
			OpenFileDialog openDiag = new OpenFileDialog()
			{
				Filter = "JSON|*.json",
			};

			if ( openDiag.ShowDialog() == DialogResult.OK )
			{
				using ( StreamReader sr = new StreamReader( openDiag.FileName ) )
				{
					ServiceList = JsonConvert.DeserializeObject<List<ServiceModel>>( sr.ReadToEnd() );
				}
			}

			ServiceList.ForEach( m => ServiceListBox.Items.Add( m.ToString() ) );
		}

		public void SetServiceURL( string URL )
		{
			if ( UseMonitorCHK.Checked )
			{
				CurrentServiceURL = URL;
			}
		}
	}
}
