﻿using MongoDB.Driver;
using System;
using TMS.Monitor;

namespace TMS
{
	public class MongoContext
	{
		public static void InsertRecord( string Collection, ServicePerformanceRecordModel Model )
		{
            // Uncomment to disable mongodb
            //return;

            MongoClient client = new MongoClient( "mongodb://localhost:27017" );

			var db = client.GetDatabase( "tms_live" );
			var collection = db.GetCollection<ServicePerformanceRecordModel>(Collection);

			collection.InsertOneAsync( Model );
		}
	}
}