﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Monitor
{
	public class ServiceModel
	{
		#region properties
		// Service Name
		public string Name { get; set; }
		// Service URL
		public string URL { get; set; }
		#endregion

		#region Overrides
		public override string ToString()
		{
			return string.Format( "{0} | {1}", Name, URL );
		}
		#endregion
	}
}
