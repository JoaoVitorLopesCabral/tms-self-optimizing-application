﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS.Monitor
{
	/// <summary>
	/// Handles service monitoring and analysis
	/// </summary>
	public class ServicePerformanceMonitor
	{
		#region Properties
		/// <summary>
		///  List of services to monitor (URLs)
		/// </summary>
		public List<ServiceModel> Services { get; set; }
		/// <summary>
		/// Service performance entries
		/// </summary>
		public List<ServicePerformanceRecordModel> Entries { get; set; }
		/// <summary>
		/// Service monitor instance
		/// </summary>
		public ServicePerformanceRecorder Recorder { get; set; }
		/// <summary>
		/// Best service available
		/// </summary>
		private ServiceStatusRecordModel BestService { get; set; }

		private IServiceConsumer ServiceConsumer { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Initialize <see cref="Recorder"/> as a new <see cref="ServicePerformanceRecorder"/> instance
		/// </summary>
		private void InitializeMonitor()
		{
			Recorder = new ServicePerformanceRecorder( Services );
			Recorder.ServicePerformanceRecordCreated += Recorder_PerformanceRecordCreated;
		}
		/// <summary>
		/// Starts the service monitor
		/// </summary>
		public void StartRecorder()
		{
			if ( Recorder == null )
			{
				InitializeMonitor();
			}

			Recorder.Start();
		}
		/// <summary>
		/// Stops the monitor
		/// </summary>
		public void StopRecorder()
		{
			if ( Recorder != null )
			{
				Recorder.Stop();
			}
		}
		/// <summary>
		/// Analyses historical data and returns the best service
		/// </summary>
		/// <returns></returns>
		public string GetBestService()
		{
			IEnumerable<ServicePerformanceRecordModel> Records = from Record in Entries
																 where ( DateTime.Now - Record.ReceiveTime ).TotalMinutes < 5
																 select Record;
		
			var GroupedRecords = Records.GroupBy( R => R.ServiceURL );
			
			List<ServiceStatusRecordModel> ServicesStatus = new List<ServiceStatusRecordModel>();
			foreach ( var ServiceGroups in GroupedRecords )
			{
				double ServicePerf = ServiceGroups.Average( R => ( R.ReceiveTime - R.SendTime ).TotalMilliseconds );
				ServicesStatus.Add( new ServiceStatusRecordModel( ServiceGroups.Key, ServicePerf ) );
			}

			return ServicesStatus.OrderBy( S => S.ResponseTime ).DefaultIfEmpty(
				new ServiceStatusRecordModel( string.Empty, 0 ) ).First().Service;
		}
		#endregion

		#region Events
		/// <summary>
		/// Event raised when a new ServicePerformanceRecord is added
		/// to <see cref="Entries"/> property
		/// </summary>
		public event PerformanceRecordCreatedEventHandler PerformanceRecordCreated;
		/// <summary>
		/// Event raised when the best service available changed
		/// </summary>
		public event ServiceChangedEventHandler BestServiceChanged;
		#endregion

		#region Event Handlers
		/// <summary>
		/// Code executed when the monitor creates a new PerformanceRecord
		/// </summary>
		/// <param name="Args"></param>
		private void Recorder_PerformanceRecordCreated( ServiceRecorderEventArgs Args )
		{
			// Add record to entries
			Entries.Add( Args.Record );

			// Raise PerformanceRecordCreated event
			PerformanceRecordCreated?.Invoke( new ServiceRecorderEventArgs( Args.Record ) );

			if ( Entries.Count >= 15 )
			{
				string BestService = GetBestService();

				ServiceConsumer.SetServiceURL( BestService );
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// Create a new <see cref="ServicePerformanceMonitor"/> instance
		/// </summary>
		public ServicePerformanceMonitor( IServiceConsumer ServiceConsumer  )
		{
			Services = new List<ServiceModel>();
			Entries = new List<ServicePerformanceRecordModel>();
			// Initialize a new monitor
			InitializeMonitor();

			this.ServiceConsumer = ServiceConsumer;
		}
		/// <summary>
		/// Create a new <see cref="ServicePerformanceMonitor"/> instance
		/// </summary>
		/// <param name="Services">List of services to monitor</param>
		public ServicePerformanceMonitor( List<ServiceModel> Services, IServiceConsumer ServiceConsumer )
		{
			this.Services = Services;
			Entries = new List<ServicePerformanceRecordModel>();
			InitializeMonitor();
			this.ServiceConsumer = ServiceConsumer;
		}
		#endregion
	}

	public class ServiceChangedEventArgs : EventArgs
	{
		#region Properties
		/// <summary>
		/// Service status record
		/// </summary>
		public ServiceStatusRecordModel Record { get; set; }
		#endregion

		#region Constructor
		/// <summary>
		/// Initializaes a new instance of the ServiceChangedEventArgs
		/// </summary>
		/// <param name="Record"></param>
		public ServiceChangedEventArgs( ServiceStatusRecordModel Record )
		{
			this.Record = Record;
		}
		#endregion
	}

	/// <summary>
	/// Event handler for when the analyser received a new performance record
	/// </summary>
	/// <param name="Args"></param>
	public delegate void PerformanceRecordCreatedEventHandler( ServiceRecorderEventArgs Args );
	/// <summary>
	/// Event handler for when the analyser changes the best service available
	/// </summary>
	/// <param name="Args"></param>
	public delegate void ServiceChangedEventHandler( ServiceChangedEventArgs Args );
}