﻿using MongoDB.Bson;
using System;

namespace TMS.Monitor
{
	/// <summary>
	/// Data structure for a recorded service performance 
	/// </summary>
	public class ServicePerformanceRecordModel
	{
		#region Properties
		public ObjectId Id { get; set; }
		/// <summary>
		/// Request send time
		/// </summary>
		public DateTime SendTime { get; set; }
		/// <summary>
		/// Request receive time
		/// </summary>
		public DateTime ReceiveTime { get; set; }
		/// <summary>
		/// Service URL
		/// </summary>
		public string ServiceURL { get; set; }
		/// <summary>
		/// Total milliseconds between send and receive
		/// </summary>
		public double TotalMilliseconds { get; set; }

		public string TotalMillisecondsString { get; set; }
        /// <summary>
        /// Service name
        /// </summary>
        public string ServiceName { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Creates a string representation of this instance
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format( "Service: {0} - Sent at: {1} | Received at: {2} | Total: {3} ms", ServiceURL,
				SendTime.ToString(), ReceiveTime.ToString(), ( ReceiveTime - SendTime ).TotalMilliseconds );
		}
		#endregion

		#region Constructor
		/// <summary>
		/// Create a new empty instance of ServicePerformanceRecord
		/// </summary>
		public ServicePerformanceRecordModel() { }
		/// <summary>
		/// Create a new instance of ServicePerformanceRecord
		/// </summary>
		/// <param name="SendTime">Timestamp when the request was sent</param>
		/// <param name="ReceiveTime">Timestamp when the response was received</param>
		/// <param name="ServiceURL">URL tested</param>
		public ServicePerformanceRecordModel( DateTime SendTime, DateTime ReceiveTime, string ServiceURL )
		{
			this.SendTime = SendTime;
			this.ReceiveTime = ReceiveTime;
			this.ServiceURL = ServiceURL;
		}
		#endregion
	}
}