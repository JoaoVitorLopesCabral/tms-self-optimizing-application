﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Globalization;

namespace TMS.Monitor
{
	/// <summary>
	/// Provides mechanisms to monitor a remote HTTP service
	/// </summary>
	public class ServicePerformanceRecorder
	{
		#region Properties
		/// <summary>
		/// Service URLs to monitor
		/// </summary>
		public List<ServiceModel> Services { get; set; }
		/// <summary>
		/// Gets the monitor running status
		/// </summary>
		public bool IsRunning { get; private set; }
		#endregion

		#region Events
		/// <summary>
		/// Event raised when a <see cref="ServicePerformanceRecordModel"/> instance is
		/// obtained using the monitor
		/// </summary>
		public event ServiceMonitorEventHandler ServicePerformanceRecordCreated;
		#endregion

		#region Methods
		/// <summary>
		/// Starts the service monitor
		/// 
		/// A background worker will be initialized to monitor each service
		/// in the <see cref="IsRunning"/> attribute
		/// </summary>
		public void Start()
		{
			// Do nothing if no services are listed
			if ( Services.Count == 0 )
			{
				return; 
			}

			// Set monitor as running
			IsRunning = true;

			// Create a single worker to handle service monitoring
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += ServiceRecorder_Worker;
			backgroundWorker.RunWorkerAsync( Services );
		}
		/// <summary>
		/// Stop monitor execution
		/// </summary>
		public void Stop()
		{
			// Change monitor running flag
			// This will tell the workers to stop
			IsRunning = false;
		}
		#endregion

		#region Worker Methods
		/// <summary>
		/// Event handler for DoWork (Service Monitor threads)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="Args"></param>
		private void ServiceRecorder_Worker( object sender, DoWorkEventArgs Args)
		{
			// Do not start monitor if argument is invalid
			// TODO: Add url validation
			List<ServiceModel> ServiceModels = (List<ServiceModel>)Args.Argument;
			if ( ServiceModels.Count == 0 )
			{
				return;
			}

			while ( IsRunning )
			{
				foreach ( ServiceModel ServiceModel in ServiceModels )
				{
					// Keep running until told otherwise
					DateTime sendTime = DateTime.Now; // Store initial timestamp

					bool CommStatus = false;

					// Request service
					using ( WebClient webClient = new WebClient() )
					{
						try
						{
							using ( Stream webStream = webClient.OpenRead( ServiceModel.URL ) )
							{
								byte[] buffer = new byte[ 100 ]; // TODO: Optimize buffer size
								int bytesRead = webStream.Read( buffer, 0, buffer.Length ); // We need only the response time
								if ( bytesRead > 0 )
								{
									CommStatus = true;
								}
							}
						}
						catch ( WebException ex )
						{
							// TODO: Log or something
						}
					}

					// Store ending timestamp
					DateTime receiveTime = DateTime.Now;

					if ( CommStatus )
					{
						// Raise event (if available)
						if ( ServicePerformanceRecordCreated != null )
						{
							ServicePerformanceRecordModel PerfRecord = new ServicePerformanceRecordModel()
							{
								SendTime = sendTime,
								ReceiveTime = receiveTime,
								ServiceURL = ServiceModel.URL,
								TotalMilliseconds = (receiveTime - sendTime).TotalMilliseconds,
								TotalMillisecondsString	= ( receiveTime - sendTime ).TotalMilliseconds.ToString( new NumberFormatInfo() { NumberDecimalSeparator = "," } ),
                                ServiceName = ServiceModel.Name
							};

							ServicePerformanceRecordCreated( new ServiceRecorderEventArgs( PerfRecord ) );
						}
					}
				}
			}
		}
		#endregion

		#region Constructor
		public ServicePerformanceRecorder()
		{
			// Not running yet
			IsRunning = false;

			// Empty service list
			Services = new List<ServiceModel>();
		}

		public ServicePerformanceRecorder( List<ServiceModel> Services )
		{
			// Not running yet
			IsRunning = false;

			// Assign service list
			this.Services = Services;
		}
		#endregion
	}
	/// <summary>
	/// Provides arguments for handling events
	/// raised by the ServiceMonitor class
	/// </summary>
	public class ServiceRecorderEventArgs : EventArgs
	{
		#region Properties
		/// <summary>
		/// Performance record received
		/// </summary>
		public ServicePerformanceRecordModel Record { get; set; }
		#endregion

		#region Constructor
		/// <summary>
		/// Initialize a new instance of ServiceMonitorEventArgs
		/// </summary>
		/// <param name="Record">Record object to send to observers</param>
		public ServiceRecorderEventArgs( ServicePerformanceRecordModel Record )
		{
			this.Record = Record;
		}
		#endregion
	}
	/// <summary>
	/// Event handler for Service Monitor events
	/// </summary>
	/// <param name="Args"></param>
	public delegate void ServiceMonitorEventHandler( ServiceRecorderEventArgs Args );
}