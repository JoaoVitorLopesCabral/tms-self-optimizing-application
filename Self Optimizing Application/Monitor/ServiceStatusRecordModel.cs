﻿using System;

namespace TMS.Monitor
{
	/// <summary>
	/// Represent a service current status
	/// </summary>
	public class ServiceStatusRecordModel
	{
		#region Properties
		/// <summary>
		/// Service URL
		/// </summary>
		public string Service { get; set; }
		/// <summary>
		/// Average service response time
		/// </summary>
		public double ResponseTime { get; set; }
		#endregion

		#region Constructor
		/// <summary>
		/// Create a new ServiceStatusRecord instance
		/// </summary>
		/// <param name="Service">Service URL</param>
		/// <param name="ResponseTime">Service average response time</param>
		public ServiceStatusRecordModel( string Service, double ResponseTime )
		{
			this.Service = Service;
			this.ResponseTime = ResponseTime;
		}
		#endregion
	}
}